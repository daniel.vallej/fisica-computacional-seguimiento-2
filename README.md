IMPLEMENTACIÓN DEL JUEGO DE LA VIDA PARA 2 ESPECIES


Integrantes y contactos

Jamir Moreno Salazar - jamir.morenos@udea.edu.co 

Juan Sebastián Sanchez Guarnizo - jsebastian.sanchez@udea.edu.co

Daniel Valle Jaramillo -  daniel.vallej@udea.edu.co



El siguiente código fue implementado en base a las siguientes premisas:

1. Implementar el juego de la vida con dos especies.

2. Modificar a su gusto alguna de las siguientes reglas (solo una de ellas, no se aceptan modificaciones de otras reglas del juego):
Condición de nacimiento: Pueden cambiar los números requeridos para que una célula muerta nazca como célula tipo A o B.

Condición de muerte: Pueden decidir incrementar la competencia, es decir, cambiar la condición de que si una célula tiene pocos vecinos de otro tipo de célula entonces muere. Ejemplo, si una célula tipo A tiene solo un vecino tipo B muere. 

Condición adicional de competencia: Pueden incluir una condición adicional en la que una célula se alimente de otra para incluir una dinámica de cazador-presa. Por ejemplo, si una célula A está rodeada por dos células B esta cambia a ser célula tipo B. Sería similar a considerar que las células B fagocitaron la célula A y se produjeron una nueva célula B en su lugar.

3. Obtener las distribuciones (histogramas normalizados a la unidad) del número de células A y B para todo el histórico de 1000 iteraciones. Hint: Pueden utilizar:

import matplotlib.pyplot as pltplt.hist(...)
fig, ax = plt.subplots()
_ = ax.hist2d(...)


4. Considere el sistema como descrito por el estado NA y NB, donde NA es el número de células tipo A y NB es el número de células tipo B. Si llamamos a NT el número total de casillas del tablero del juego, tenga en cuenta que NA+NB<=NT. De la misma forma el número de células muertas es NT-NA-NB. Además, tenga presente que como las dos especies de células interactúan es previsible que la probabilidad de encontrar un número de células A no sea independiente de encontrar un número de células B, en otras palabras NA y NB están correlacionados, y, por lo tanto, lo mejor es utilizar la probabilidad 2-dimensional como distribución de probabilidad de los estados del sistema y no las probabilidades marginales de cada variable separadamente.

5. Utilizando las distribuciones obtenidas en el punto 3 y teniendo en cuenta lo señalado en el punto 4, utilice el algoritmo de Metrópolis para generar una serie de estados del sistema.

6. Grafique la probabilidad 2-dimensional de los estados generados con el algoritmo de Metrópolis y muestre cualitativamente que la distribución obtenida es similar a la del punto 3.

DESARRROLLO:

Suponga que tiene un tablero compuesto por 2 especies de células A,B y células las muertas las cuales se denotan como espacios vacíos en el tablero. Con esto establecemos unas condiciones iniciales para las cuales puede haber supervivencia, muerte o nacimiento de A o B tal que:

Supervivencia:

- Una especia A sobrevive si tiene entre 2 y 3 vecinos de su misma especie y no mas de 2 vecinos de la especie B.

- Una especie B sobrevive si tiene entre 2 y 3 vecinos de su misma especie y no mas de 2 vecinos de la especie A.

Muerte:

-Una célula de A muere si tiene mas de 3 vecinos de A o si tiene menos de 2 vecinos de A o si tiene mas de 2 vecinos de B.

-Una célula de B muere si tiene mas de 3 vecinos de B o si tiene menos de 2 vecinos de B o si tiene mas de 2 vecinos de A.

Nacimiento:

- Una célula muerta nace como A si tiene exactamente 3 vecinos de A y menos de 2 vecinos de B.

- Una célula muerta nace como B si tiene exactamente 3 vecinos de B y menos de 2 vecinos de A.

Por otro lado elegimos una condición adicional de competencia:

Fagocitación:

-Para esta condición consideraremos que una célula se alimente de otra (lo que incluye una dinámica de cazador-presa), de manera que esto se realizara al considerar que las células B puede fagocitar a las células A produciendo una nueva célula B en su lugar. Así, A es fagocitada si esta rodeada por 2 o mas células de la especie B.

En base a esto se implementó un programa en Python el cual recogía por un lado las distribuciones de las células A y B y la matriz de probabilidad dado el juego de la vida para un número determinado de iteraciones. Por otro lado se realizo un procedimiento análogo pero usando el algoritmo de metrópolis para así ver como se podían asemejar ambas versiones. En el presente repositorio se adjuntan los gráficos para ambos casos.

A partir de esto podemos inferir que para un número lo suficientemente alto de iteraciones en el algoritmo de metrópolis ( En este caso 1000), este mismo tiene una gran semejanza con el juego de la vida dadas las condiciones previamente descritas, sin embargo, ha de acotarse que dado que metrópolis funciona mediante la "aleatoriedad" entonces cada intento podría parecerse mas o menos a lo que se podría esperar a priori. Así, al trabajar con una muestra podríamos intentar modelar el comportamiento a futuro del juego de la vida, esta ventaja nos la otorga metrópolis y hace el desarrollo mucho mas practico que el primer método, aunque como se menciono previamente sus resultados no son iguales pero si análogos a medida que avanza el tiempo.

A manera de cierre se enuncian las librerías necesarias para el desarrollo del código :

import time

import random

import numpy as np

import matplotlib.pyplot as plt

from matplotlib.colors import Normalize

from matplotlib.cm import ScalarMappable

from IPython.display import clear_output





 